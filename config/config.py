import json


class Config:
    __slots__ = (
        "get_data",
        "indicators",
        "target",
        "prepare_data",
        "models",
        "mysql",
        "backtests",
        "api_key",
        "api_secret",
    )

    def handle_error(self, exc):
        print(exc)

    def __init__(self, config_name="config.json"):
        try:
            settings_file = f"./config/{config_name}"
            user_settings = json.load(open(settings_file))

            for user_setting_key in user_settings:
                setattr(self, user_setting_key, user_settings[user_setting_key])

        except IndexError as exc:
            self.handle_error(f"Provide config name. Exception: {exc}")
        except FileNotFoundError as exc:
            self.handle_error(f"File not found. Exception: {exc}")
        except json.JSONDecodeError as exc:
            self.handle_error(f"Json error. Exception: {exc}")
        except AttributeError as exc:
            self.handle_error(f"No setting available. Exception: {exc}")


import config.config as config
from binance import Client
from src.bot import Bot
import threading
import time
import sys

# import nest_asyncio
# nest_asyncio.apply()
def main():
    try:
        sett = config.Config(str(sys.argv[1]))
        ticks = sett.get_data["ticks"]
        binanceClient = Client(sett.api_key, sett.api_secret)
        timeframe = sett.get_data["timeframe"]
        bots = []
        for tick in ticks:
            bots.append(
                Bot(1, binanceClient, tick, timeframe, sett, 30, str(sys.argv[1]))
            )
        for objBot in bots:
            run_threaded(objBot.startTraderBot)
            time.sleep(300)
        while 1:
            time.sleep(60)
    except ValueError as ve:
        return str(ve)


def run_threaded(job_func):
    print("### Iniciando nuevo hilo de ejecución")
    job_thread = threading.Thread(target=job_func, daemon=True)
    job_thread.start()


if __name__ == "__main__":
    main()

import config.config as config
from binance import Client
import pandas as pd
import sys

def main():
    try:
        sett = config.Config(str(sys.argv[1]))
        ticks = sett.get_data["ticks"]
        binanceClient = Client(sett.api_key, sett.api_secret)

        candles = binanceClient.get_klines(symbol=ticks, interval="1m", limit=1)
        lr = pd.DataFrame(candles)
        print(lr)

    except ValueError as ve:
        return str(ve)


if __name__ == "__main__":
    main()

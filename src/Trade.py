# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 22:03:22 2022

@author: x4vyjm
"""


class Trade:

    accumulation_id = None
    entry_price = None
    close_price = None
    quantity = None
    inversion = None
    closed = 0
    open_date = ""
    close_date = ""
    order_id = ""
    period = 1
    conn = None
    trade_id = None

    def __init__(
        self,
        accumulation_id,
        entry_price,
        quantity,
        inversion,
        open_date,
        order_id,
        conn,
        new=True,
        trade_id=None,
    ):
        print(f"### Registrando nuevo trade")
        self.accumulation_id = accumulation_id
        self.entry_price = entry_price
        self.quantity = quantity
        self.inversion = inversion
        self.open_date = open_date
        self.order_id = order_id
        self.conn = conn
        if new:
            print("### Trade nuevo creado")
            command = """CREATE TABLE IF NOT EXISTS TRADE (id integer primary key autoincrement, accumulation_id integer, entry_price float,close_price float, 
                                                        quantity float, inversion float, closed integer, open_date timestamp, 
                                                        close_date timestamp, order_id text)"""
            cur = self.conn.cursor()
            cur.execute(command)
            self.conn.commit()
            sql = f"""INSERT INTO TRADE (accumulation_id, entry_price, quantity, inversion, closed, open_date, order_id) 
                    VALUES({int(self.accumulation_id)},{float(self.entry_price)},{float(self.quantity)},{float(self.inversion)},{int(self.closed)},"{str(self.open_date)}","{str(self.order_id)}")"""
            cur = self.conn.cursor()
            cur.execute(sql)
            self.trade_id = cur.lastrowid
            self.conn.commit()
        else:
            print(f"### Trade existente con id {trade_id} cargado")
            self.trade_id = trade_id

    def close(self, close_price, close_date):
        print("### Cierre de trade")
        self.closed = 1
        self.close_date = close_date
        self.close_price = close_price
        sql = f"""Update TRADE set closed = 1, close_date = "{str(close_date)}", close_price = {float(close_price)} where id = {int(self.trade_id)}"""
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()

    def isClosed(self):
        print(f"### Obteniendo informacion si el trade es cerrado")
        return self.closed


# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 19:36:14 2022

@author: x4vyjm
"""
from src.prepareData import PrepareData
from strategies.Strategies import AcumulatorStrategy
import joblib
import datetime
import ast
import numpy as np
import pandas as pd


class Trader:
    """
    Main Class thqt
    """

    models = []
    scalers = []
    dataset = None
    conn = None
    bc = None
    timeframe = ""
    pair = ""
    objData = None
    configuration = None
    scaler = None
    n_decimals = 0

    def __init__(self, BinanceClient, timeframe, pair, configuration):
        """
        Parameters
        ----------
        BinanceClient : object
            Binance client object.
        timeframe : string
            Timeframe for tick.
        pair : string
            Ticker or symbol for trade
        configuration : object
            Configuration object.
        Returns
        -------
        None.
        """
        self.bc = BinanceClient
        self.timeframe = timeframe
        self.pair = pair
        self.configuration = configuration
        self.objData = PrepareData(
            pair, timeframe, configuration.indicators, limit=True
        )
        self.conn = self.objData.conn
        self.getBestResult()
        self.check_decimals()
        self.objStrategy = AcumulatorStrategy(
            self.configuration.backtests[0],
            self.pair,
            self.timeframe,
            self.conn,
            self.n_decimals,
            self.bc,
        )

    def getBestResult(self, column="profit"):
        """
        Parameters
        ----------
        column : string, optional
            Get the best train by "column" (ie. 'profit', 'f1' or 'roc'). The default is "profit".
            Then loads the trained models in an array (models) also load the scaler (scaler) used for training.
        Returns
        -------
        None.

        """
        cur = self.conn.cursor()
        command = "SELECT * FROM MODEL ORDER BY " + str(column) + " DESC LIMIT 1"
        cur.execute(command)
        rows = cur.fetchall()
        self.models = []
        self.scalers = []
        # if "All" in str(rows[0][2]):
        #    for mod in ast.literal_eval(str(rows[0][3])):
        #        print(mod)
        #        self.models.append(joblib.load(mod))
        # else:
        for row in rows:
            self.models.append(joblib.load(row[12]))
            self.scalers.append(joblib.load(row[13]))
        # self.scaler = joblib.load(rows[0][4])

        self.configuration.backtests[0] = ast.literal_eval(str(rows[0][8]))
        self.configuration.target = ast.literal_eval(str(rows[0][9]))
        self.configuration.indicators = ast.literal_eval(str(rows[0][23]))

    def checkLastCandle(self):
        """
        Returns
        -------
        ds : dataset
            last row dataframe with buy signal.
        """
        print("### Revisar ultima vela")
        now = datetime.datetime.now()
        time_divisor = {
            "5m": 5,
            "30m": 30,
            "15m": 15,
        }
        exact_timeframe = now.minute % time_divisor[self.timeframe] == 0
        if exact_timeframe:
            print("### Revisar ultima vela: llamada cada timeframe")
            self.objData.loadDataset(True)
            lr = self.objData.getLastRow()
            lr["open_time"] = pd.to_datetime(lr["open_time"], unit="ms")
            lr["close_time"] = pd.to_datetime(lr["close_time"], unit="ms")
            lr = lr.set_index("open_time")
            X_col = lr[self.objData.getXColumns()]
            flag = 0
            aux1 = None
            index = 0
            for model in self.models:
                ds = self.scalers[index].transform(X_col)
                pred = model.predict(ds)
                aux = np.where(pred == True, 1, 0)
                if flag == 0:
                    aux1 = aux
                    flag = 1
                else:
                    aux1 += aux
                index += 1
            lr["buy"] = np.where(aux1 >= 1, True, False)
            print(lr)
        else:
            print("### Revisar ultima vela: llamada cada minuto")
            candles = self.bc.get_klines(symbol=self.pair, interval="1m", limit=1)
            lr = pd.DataFrame(candles)
            lr.columns = [
                "open_time",
                "open",
                "high",
                "low",
                "close",
                "volume",
                "close_time",
                "Quote asset volume",
                "Number of trades",
                "Taker buy base asset volume",
                "Taker buy quote asset volume",
                "ignore",
            ]
            lr = lr[["close"]]
            lr["buy"] = False
        self.objStrategy.process(lr, exact_timeframe)

    def check_decimals(self):
        print("### Obteniendo informacion de decimales")
        info = self.bc.get_symbol_info(self.pair)
        val = info["filters"][2]["stepSize"]
        decimal = 0
        is_dec = False
        for c in val:
            if is_dec is True:
                decimal += 1
            if c == "1":
                break
            if c == ".":
                is_dec = True
        self.n_decimals = decimal

# -*- coding: utf-8 -*-
"""
Created on Mon Apr 11 16:49:57 2022

@author: x4vyjm
"""
import pandas as pd
import numpy as np
import sqlite3 as sql
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn import svm
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split, cross_val_score, GridSearchCV
from sklearn.metrics import plot_roc_curve, f1_score, roc_auc_score
from sklearn.metrics import accuracy_score, classification_report
from sklearn.metrics import (
    confusion_matrix,
    plot_confusion_matrix,
    ConfusionMatrixDisplay,
)
import xgboost as xgb
import joblib
import strategies.backtestStrategies as bs
from plotly.subplots import make_subplots
import plotly.graph_objects as go
import plotly.express as px
from datetime import date, datetime
import json
from src.prepareData import PrepareData
import ast
import os


class Trainer:
    dataset = None
    u_dataset = None
    models = []
    backtest = []
    scaler = None
    trained_models = []
    x_columns = []
    x_columns_except_price = []
    y_column = ""
    only_oversell = True
    test_size = 0.2
    below_rsi = 28
    random_state = 0
    X_train = None
    X_test = None
    y_train = None
    y_test = None
    db_name = ""
    conn = None
    train_id = 0
    train_model = ""
    pair = ""
    timeframe = ""
    configuration = []
    sql_file = None

    def __init__(self, configuration, pair, timeframe, train_id):

        self.configuration = configuration
        self.pair = pair
        self.timeframe = timeframe
        self.train_name = pair + timeframe
        self.train_id = train_id
        self.db_name = "./database/" + pair + timeframe + ".db"
        self.sql_file = "./database/" + pair + timeframe + ".txt"
        self.conn = sql.connect(self.db_name)
        cur = self.conn.cursor()
        command = """CREATE TABLE IF NOT EXISTS MODEL (id integer primary key autoincrement, train_id integer, 
                                                        model_name text, tp integer, fp integer, f1 float, precision float, 
                                                        profit float, backtest_conf text, target_conf text, chart_fondos text, 
                                                        chart_candles text,  model_file text,scaler_file text, parameters text, 
                                                        best_params text, accuracy float, recall float, specificity float, 
                                                        roc float, tn integer, fn integer, backtest_name text, indicator_conf text, prepare_conf text,
                                                        n_buys integer, n_buysacum integer, type_evaluation text)"""
        cur.execute(command)
        self.conn.commit()

    def splitData(self):
        if self.only_oversell:
            print(f"#### ELIMINANDO DATA CON RSI > {self.below_rsi} ####")
            datasetX = self.dataset.drop(
                self.dataset[(self.dataset["rsi"] > self.below_rsi)].index
            )
            X_Cols = datasetX[self.x_columns_except_price]
            Y_Cols = datasetX[self.y_column]
        else:
            X_Cols = self.dataset[self.x_columns_except_price]
            Y_Cols = self.dataset[self.y_column]

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(
            X_Cols, Y_Cols, test_size=self.test_size, random_state=self.random_state
        )
        self.scaler = StandardScaler()
        self.X_train = self.scaler.fit_transform(self.X_train)
        self.X_test = self.scaler.transform(self.X_test)

    def predictAllModels(self, dataset, escalado=False):
        if escalado:
            dataset = self.scaler.transform(dataset)
        index = 0
        aux1 = None
        for model in self.trained_models:
            pred = model["model"].predict(dataset)
            aux = np.where(pred == True, 1, 0)
            if index == 0:
                aux1 = aux
                index = 1
            else:
                aux1 += aux
        self.y_pred = np.where(aux1 >= 3, True, False)

    def predictOneModel(self, model, dataset, escalado=False):
        if escalado:
            dataset = self.scaler.transform(dataset)
        self.y_pred = model.predict(dataset)

    def train(self):
        for config in self.configuration:
            print("--------- PREPARACION DE DATOS ---------")
            objPrepare = PrepareData(
                self.pair,
                self.timeframe,
                config.indicators,
                config.get_data["ini"],
                config.get_data["end"],
                config.prepare_data["date_split"],
                config.prepare_data["graph_start"],
                config.prepare_data["graph_end"],
                config.target,
            )
            self.dataset = objPrepare.getTrainDataset()
            self.u_dataset = objPrepare.getUnknownDataset()
            self.models = config.models
            self.backtests = config.backtests
            self.x_columns = objPrepare.getXColumns()
            self.x_columns_except_price = objPrepare.getXColumnsExceptPrice()
            self.y_column = "resultado"
            self.only_oversell = config.prepare_data["only_oversell"]
            self.test_size = config.prepare_data["test_size"]
            self.random_state = config.prepare_data["random_state"]
            self.below_rsi = config.prepare_data["below_rsi"]
            self.splitData()
            # self.trained_models = []
            nombre_scaler = (
                "./models/scalers/"
                + "_"
                + self.pair
                + "_"
                + self.timeframe
                + "_"
                + datetime.now().strftime("%d%m%Y%I%M%S%p")
                + ".joblib"
            )
            joblib.dump(self.scaler, nombre_scaler, 3)
            escalador_usado = 0
            print("--------- ENTRENAMIENTO ---------")
            # nombre_modelos = []
            for mod in self.models:
                if mod["use"]:
                    if mod["name"] == "random_forest":
                        model, best_params, nombre_modelo = self.entrenarRF(mod)
                    elif mod["name"] == "knn":
                        model, best_params, nombre_modelo = self.entrenarKNN(mod)
                    elif mod["name"] == "svm":
                        model, best_params, nombre_modelo = self.entrenarSVM(mod)
                    elif mod["name"] == "xgb":
                        model, best_params, nombre_modelo = self.entrenarXGB(mod)
                    self.predictOneModel(model, self.X_test)
                    accu, prec, rec, spec, f1, roc, tn, tp, fn, fp = self.metrics(
                        self.y_test, self.y_pred
                    )

                    # self.predictOneModel(model, datasetX[self.x_columns], True)
                    # accu, prec, rec, spec, f1, roc, tn, tp, fn, fp = self.metrics(
                    #    datasetX[self.y_column], self.y_pred
                    # )
                    self.predictOneModel(
                        model, self.u_dataset[self.x_columns_except_price], True
                    )  # cambiar x_columns
                    if float(f1) > 0.75:
                        print(f"f1 ({f1}) mayor a 0.7")
                        self.u_dataset["predecido"] = self.y_pred
                        for strat in self.backtests:
                            (
                                chart_fondos,
                                chart_candles,
                                backtest_name,
                                backtest_conf,
                                profit,
                                n_buys,
                                n_buysacum,
                            ) = self.backtest(self.u_dataset, mod["name"], strat)
                            # nombre_modelos.append(nombre_modelo)
                            # self.trained_models.append(
                            #    {
                            #        "name": mod["name"],
                            #        "model": model,
                            #        "best_params": best_params,
                            #    }
                            # )
                            if (
                                float(profit)
                                > float(strat["parameters"]["initial_capital"]) * 1.02
                            ):
                                sql = f"""INSERT INTO MODEL(train_id,model_name,model_file,scaler_file,parameters,best_params,
                                f1,accuracy,precision,recall,specificity,roc,tn,tp,fn,fp,
                                backtest_name,backtest_conf,indicator_conf, prepare_conf, target_conf,profit,n_buys,n_buysacum,
                                chart_fondos,chart_candles, type_evaluation)
                                VALUES({int(self.train_id)},"{str(mod['name'])}","{str(nombre_modelo)}","{str(nombre_scaler)}","{str(mod['parameters'])}",
                                "{str(best_params)}",{f1},{accu},{prec},{rec},{spec},{roc},{tn},{tp},{fn},
                                {fp}, "{str(backtest_name)}", "{str(backtest_conf)}", "{str(config.indicators)}", "{str(config.prepare_data)}", "{str(config.target)}", {float(profit)},
                                {int(n_buys)}, {int(n_buysacum)}, "{str(chart_fondos)}", "{str(chart_candles)}", 'One models')"""

                                cur = self.conn.cursor()
                                cur.execute(sql)
                                self.conn.commit()
                                f = open(self.sql_file, "a")
                                f.write(sql + ";")
                                f.close()
                                escalador_usado += 1
                            else:
                                # eliminar el modelo entrenado para ahorrar espacio
                                if os.path.exists(nombre_modelo):
                                    os.remove(nombre_modelo)
                    else:
                        # eliminar el modelo entrenado para ahorrar espacio
                        if os.path.exists(nombre_modelo):
                            os.remove(nombre_modelo)
            if escalador_usado == 0:
                # eliminar el escalador para ahorrar espacio
                if os.path.exists(nombre_scaler):
                    os.remove(nombre_scaler)
            # self.predictAllModels(self.X_test)
            # accu, prec, rec, spec, f1, roc, tn, tp, fn, fp = self.metrics(self.y_pred)
            # self.predictAllModels(self.u_dataset[self.x_columns], True)
            # self.u_dataset["predecido"] = self.y_pred
            # (
            #    chart_fondos,
            #    chart_candles,
            #    backtest_name,
            #    backtest_conf,
            #    profit,
            #    n_buys,
            #    n_buysacum,
            # ) = self.backtest(self.u_dataset, "All")
            # sql = f"""INSERT INTO MODEL(train_id,model_name,model_file,scaler_file,parameters,best_params,
            # f1,accuracy,precision,recall,specificity,roc,tn,tp,fn,fp,
            # backtest_name,backtest_conf,indicator_conf, prepare_conf, target_conf,profit,n_buys,n_buysacum,
            # chart_fondos,chart_candles, type_evaluation)
            # VALUES({int(self.train_id)},'All models',"{str(nombre_modelos)}","{str(nombre_scaler)}","{str(mod['parameters'])}",
            #'All models',{f1},{accu},{prec},{rec},{spec},{roc},{tn},{tp},{fn},
            # {fp}, "{str(backtest_name)}", "{str(backtest_conf)}", "{str(config.indicators)}", "{str(config.prepare_data)}", "{str(config.target)}", {float(profit)},
            # {int(n_buys)}, {int(n_buysacum)}, "{str(chart_fondos)}", "{str(chart_candles)}", 'All models')"""
            # cur = self.conn.cursor()
            # cur.execute(sql)
            # self.conn.commit()
            # f = open(self.sql_file, "a")
            # f.write(sql + ";")
            # f.close()
        # self.deleteTraineds()
        return True

    def backtest(self, dataset, model_name, strat):
        if strat["use"] or strat["use"] == "True":
            if strat["name"] == "reverse_ratio_0_5":
                strategy = bs.Reverse_ratio_0_5(
                    strat["parameters"]["initial_capital"],
                    strat["parameters"]["profit"],
                    strat["parameters"]["loss"],
                    strat["parameters"]["amount_per_trade"],
                    strat["parameters"]["distance"],
                )
                strategy.procesarDataset(dataset[["predecido", "close", "high", "low"]])
                dataset["buy"] = strategy.lista_buys
                dataset["sell"] = strategy.lista_sells
                dataset["capital"] = strategy.lista_fondos
                titulo = self.train_name + "_" + model_name + "_" + strat["name"]
                name_evol = (
                    "./data/backtest/fondos_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                name_chart = (
                    "./data/backtest/chart_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                self.chartBacktesting(dataset, name_chart, titulo)
                self.chartEvolution(
                    dataset.loc[:, ["close_time", "capital"]],
                    "close_time",
                    "capital",
                    "Capital " + titulo,
                    name_evol,
                )

            if strat["name"] == "promedio_creciente":
                strategy = bs.Promedio_creciente(
                    strat["parameters"]["initial_capital"],
                    strat["parameters"]["max_buys"],
                    strat["parameters"]["initial_divisor"],
                    strat["parameters"]["profit"],
                    strat["parameters"]["distance"],
                )
                strategy.procesarDataset(dataset[["predecido", "close", "high"]])
                dataset["buy"] = strategy.lista_buys
                dataset["sell"] = strategy.lista_sells
                dataset["capital"] = strategy.lista_fondos
                dataset["invertido"] = strategy.lista_invertido
                titulo = self.train_name + "_" + model_name + "_" + strat["name"]
                name_evol = (
                    "./data/backtest/fondos_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                name_chart = (
                    "./data/backtest/chart_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                print("Evaluando profit: ", strategy.lista_fondos[-1])
                if (
                    strategy.lista_fondos[-1]
                    > float(strat["parameters"]["initial_capital"]) * 1.02
                ):
                    self.chartBacktesting(dataset, name_chart, titulo)
                    self.chartEvolution(
                        dataset.loc[:, ["close_time", "capital"]],
                        "close_time",
                        "capital",
                        "Capital " + titulo,
                        name_evol,
                    )

                return (
                    name_evol,
                    name_chart,
                    strat["name"],
                    strat,
                    strategy.lista_fondos[-1],
                    strategy.lista_buys.count(True),
                    max(strategy.lista_periodos),
                )

            if strat["name"] == "promedio_creciente_dos":
                strategy = bs.Promedio_creciente_dos(
                    strat["parameters"]["initial_capital"],
                    strat["parameters"]["max_buys"],
                    strat["parameters"]["initial_divisor"],
                    strat["parameters"]["profit"],
                    strat["parameters"]["distance"],
                )
                strategy.procesarDataset(dataset[["predecido", "close", "high"]])
                dataset["buy"] = strategy.lista_buys
                dataset["sell"] = strategy.lista_sells
                dataset["capital"] = strategy.lista_fondos
                dataset["invertido"] = strategy.lista_invertido
                titulo = self.train_name + "_" + model_name + "_" + strat["name"]
                name_evol = (
                    "./data/backtest/fondos_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                name_chart = (
                    "./data/backtest/chart_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                self.chartBacktesting(dataset, name_chart, titulo)
                self.chartEvolution(
                    dataset.loc[:, ["close_time", "capital"]],
                    "close_time",
                    "capital",
                    "Capital " + titulo,
                    name_evol,
                )

                return (
                    name_evol,
                    name_chart,
                    strat["name"],
                    strat,
                    strategy.lista_fondos[-1],
                    strategy.lista_buys.count(True),
                    max(strategy.lista_periodos),
                )

            if strat["name"] == "promedio_creciente_tres":
                strategy = bs.Promedio_creciente_tres(
                    strat["parameters"]["initial_capital"],
                    strat["parameters"]["max_buys"],
                    strat["parameters"]["initial_divisor"],
                    strat["parameters"]["profit"],
                    strat["parameters"]["distance"],
                )
                strategy.procesarDataset(dataset[["predecido", "close", "high"]])
                dataset["buy"] = strategy.lista_buys
                dataset["sell"] = strategy.lista_sells
                dataset["capital"] = strategy.lista_fondos
                dataset["invertido"] = strategy.lista_invertido
                titulo = self.train_name + "_" + model_name + "_" + strat["name"]
                name_evol = (
                    "./data/backtest/fondos_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                name_chart = (
                    "./data/backtest/chart_"
                    + datetime.now().strftime("%d%m%Y%I%M%S%p")
                    + ".html"
                )
                self.chartBacktesting(dataset, name_chart, titulo)
                self.chartEvolution(
                    dataset.loc[:, ["close_time", "capital"]],
                    "close_time",
                    "capital",
                    "Capital " + titulo,
                    name_evol,
                )

                return (
                    name_evol,
                    name_chart,
                    strat["name"],
                    strat,
                    strategy.lista_fondos[-1],
                    strategy.lista_buys.count(True),
                    max(strategy.lista_periodos),
                )

    def chartBacktesting(self, dataset, nombre, titulo):
        """
        Recibe un dataset con data completa lista para generar el gráfico de velas
        y las marcas de compras y ventas.
        """
        # dataset1 = dataset2.loc['2022-1-1':'2022-1-30']
        print("#### IMPRIMIENDO GRÁFICA  ", titulo, " ####")
        dataset = dataset.reset_index()
        fig = make_subplots(
            rows=3,
            cols=1,
            shared_xaxes=True,
            vertical_spacing=0.01,
            row_width=[0.2, 0.1, 0.5],
        )

        fig.add_trace(
            go.Candlestick(
                x=dataset["open_time"],
                open=dataset["open"],
                high=dataset["high"],
                low=dataset["low"],
                close=dataset["close"],
            ),
            row=1,
            col=1,
        )
        fig.add_trace(
            go.Scatter(x=dataset["open_time"], y=dataset["rsi"]), row=2, col=1
        )
        fig.add_trace(
            go.Scatter(x=dataset["open_time"], y=dataset["capital"]), row=3, col=1
        )
        fig.add_trace(
            go.Scatter(x=dataset["open_time"], y=dataset["invertido"]), row=3, col=1
        )

        for i in range(len(dataset["predecido"])):
            buys = dataset.loc[i:i, "buy"].values[0]
            sells = dataset.loc[i:i, "sell"].values[0]
            res = dataset.loc[i:i, "resultado"].values[0]
            if buys == True:
                fig.add_annotation(
                    x=dataset.loc[i, "open_time"],
                    y=dataset.loc[i, "close"],
                    text="▲",
                    showarrow=False,
                    font=dict(size=16, color="blue"),
                )
            if res == True:
                fig.add_annotation(
                    x=dataset.loc[i, "open_time"],
                    y=dataset.loc[i, "close"],
                    text="◀",
                    showarrow=False,
                    font=dict(size=16, color="green"),
                )
            if sells == True:
                fig.add_annotation(
                    x=dataset.loc[i, "open_time"],
                    y=dataset.loc[i, "close"],
                    text="▲",
                    showarrow=False,
                    font=dict(size=16, color="red"),
                )
        fig.update_layout(
            xaxis_rangeslider_visible=False, showlegend=False, title=titulo
        )
        fig.write_html(nombre)
        # fig.show()

    def chartEvolution(self, dataset, x_name, y_name, titulo, nombre, todo=False):
        if not (todo):
            print("#### IMPRIMIENDO  ", titulo, " ####")
            fig = px.line(dataset, x=x_name, y=y_name, title=titulo)
            fig.write_html(nombre)
            # fig.show()
        else:
            print("#### IMPRIMIENDO  ", titulo, " TODO ####")
            df = pd.DataFrame()
            df["close_time"] = dataset[0]["close_time"]
            df["capital"] = 0.04
            df["invertido"] = 0
            for ds in dataset:
                df["invertido"] += ds["invertido"]
                df["capital"] += ds["capital"] - 0.02
            fig = make_subplots(
                rows=2, cols=1, shared_xaxes=True, vertical_spacing=0.02
            )
            fig.add_trace(go.Scatter(x=df[x_name], y=df[y_name[0]]), row=1, col=1)
            fig.add_trace(go.Scatter(x=df[x_name], y=df[y_name[1]]), row=2, col=1)
            fig.update_layout(
                xaxis_rangeslider_visible=False, showlegend=True, title=titulo
            )
            fig.write_html(nombre)
            # fig.show()

    def metrics(self, y_test, y_pred):
        print(y_test)
        print(len(y_pred))
        rf_matrix = confusion_matrix(y_test, y_pred)

        true_negatives = rf_matrix[0][0]
        false_negatives = rf_matrix[1][0]
        true_positives = rf_matrix[1][1]
        false_positives = rf_matrix[0][1]
        accuracy = (true_negatives + true_positives) / (
            true_negatives + true_positives + false_negatives + false_positives
        )
        precision = true_positives / (true_positives + false_positives)
        recall = true_positives / (true_positives + false_negatives)
        specificity = true_negatives / (true_negatives + false_positives)
        print("Verdaderos Positivos: ", true_positives)
        print("Falsos Positivos: ", false_positives)
        print("Verdaderos Negativos: ", true_negatives)
        print("Falsos Negativos: ", false_negatives)
        # print("Exactitud: {}".format(float(accuracy)))
        print("Precisión: {}".format(float(precision)))
        print("Sensibilidad: {}".format(float(recall)))
        # print("Especificidad: {}".format(float(specificity)))
        print(classification_report(y_test, y_pred))
        roc_value = roc_auc_score(y_test, y_pred)
        f1_value = f1_score(y_test, y_pred)
        print("\n\nF1 Value: ", f1_value)
        print("\n\nROC Value: ", roc_value)
        return (
            accuracy,
            precision,
            recall,
            specificity,
            f1_value,
            roc_value,
            true_negatives,
            true_positives,
            false_negatives,
            false_positives,
        )

    def entrenarRF(self, conf):
        """
        X: dataset con las variables independientes
        y: dataset con la variable dependiende
        modelo: cadena con el nombre del modelo preentrenado en la carpeta del proyecto
        """
        nombre_modelo = (
            "./models/"
            + conf["parameters"]["name"]
            + "_"
            + self.pair
            + "_"
            + self.timeframe
            + "_"
            + datetime.now().strftime("%d%m%Y%I%M%S%p")
            + ".joblib"
        )
        if conf["parameters"]["search_parameter"]["use"]:
            print("#### ENTRENANDO RANDOM FOREST CON GRIDSEARCH y CV ####")
            param_grid = {
                "n_estimators": conf["parameters"]["search_parameter"]["n_stimators"],
                "random_state": conf["parameters"]["search_parameter"]["random_state"],
                "min_samples_split": conf["parameters"]["search_parameter"][
                    "min_samples_split"
                ],
            }
            rf = RandomForestClassifier()
            rf_RandomGrid = GridSearchCV(
                estimator=rf,
                param_grid=param_grid,
                cv=conf["parameters"]["search_parameter"]["cv"],
                scoring="f1",
                n_jobs=2,
            )
            rf_RandomGrid.fit(self.X_train, self.y_train)
            print("#### cv_result f1 ####")
            print(rf_RandomGrid.cv_results_["mean_test_score"])
            print("#### Best paramters ####")
            print(rf_RandomGrid.best_params_)
            print(
                f"Train Accuracy - : {rf_RandomGrid.score(self.X_train,self.y_train):.3f}"
            )
            best_model = rf_RandomGrid.best_estimator_
            best_model.fit(self.X_train, self.y_train)
            joblib.dump(best_model, nombre_modelo, conf["parameters"]["compressor"])
            return best_model, rf_RandomGrid.best_params_, nombre_modelo
        if conf["parameters"]["new"]:
            print("#### ENTRENANDO RANDOM FOREST ####")
            rf = RandomForestClassifier(
                n_estimators=conf["parameters"]["estimators"],
                random_state=conf["parameters"]["random_state"],
            )
            rf.fit(self.X_train, self.y_train)
            joblib.dump(rf, nombre_modelo, conf["parameters"]["compressor"])
        return rf, None, nombre_modelo

    def entrenarKNN(self, conf):
        nombre_modelo = (
            "./models/"
            + conf["parameters"]["name"]
            + "_"
            + self.pair
            + "_"
            + self.timeframe
            + "_"
            + datetime.now().strftime("%d%m%Y%I%M%S%p")
            + ".joblib"
        )
        if conf["parameters"]["search_parameter"]["use"]:
            print("#### ENTRENANDO KNN CON GRIDSEARCH y CV ####")
            param_grid = {
                "n_neighbors": conf["parameters"]["search_parameter"]["n_neighbors"],
                "metric": conf["parameters"]["search_parameter"]["metric"],
            }
            grid_knn = KNeighborsClassifier()
            knn_RandomGrid = GridSearchCV(
                estimator=grid_knn,
                param_grid=param_grid,
                cv=conf["parameters"]["search_parameter"]["cv"],
                scoring="f1",
                n_jobs=2,
            )
            knn_RandomGrid.fit(self.X_train, self.y_train)
            print("#### cv_result f1 ####")
            print(knn_RandomGrid.cv_results_["mean_test_score"])
            print("#### Best paramters ####")
            print(knn_RandomGrid.best_params_)
            print(
                f"Train Accuracy - : {knn_RandomGrid.score(self.X_train,self.y_train):.3f}"
            )
            best_model = knn_RandomGrid.best_estimator_
            best_model.fit(self.X_train, self.y_train)
            joblib.dump(best_model, nombre_modelo, conf["parameters"]["compressor"])
            return best_model, knn_RandomGrid.best_params_, nombre_modelo
        if conf["parameters"]["new"]:
            print("#### ENTRENANDO KNN ####")
            model_knn = KNeighborsClassifier(
                n_neighbors=conf["parameters"]["neighbours"],
                metric=conf["parameters"]["metric"],
                p=2,
            )
            model_knn.fit(self.X_train, self.y_train)
            joblib.dump(model_knn, nombre_modelo, conf["parameters"]["compressor"])
        return model_knn, None, nombre_modelo

    def entrenarXGB(self, conf):
        nombre_modelo = (
            "./models/"
            + conf["parameters"]["name"]
            + "_"
            + self.pair
            + "_"
            + self.timeframe
            + "_"
            + datetime.now().strftime("%d%m%Y%I%M%S%p")
            + ".joblib"
        )
        if conf["parameters"]["search_parameter"]["use"]:
            print("#### ENTRENANDO XGBOOST CON GRIDSEARCH y CV ####")
            param_grid = {
                "n_estimators": conf["parameters"]["search_parameter"]["n_estimators"],
                "learning_rate": conf["parameters"]["search_parameter"][
                    "learning_rate"
                ],
                "objective": conf["parameters"]["search_parameter"]["objective"],
                "random_state": conf["parameters"]["search_parameter"]["random_state"],
            }
            grid_xgb = xgb.XGBClassifier(
                verbosity=0, silent=True, use_label_encoder=False
            )
            xgb_RandomGrid = GridSearchCV(
                estimator=grid_xgb,
                param_grid=param_grid,
                cv=conf["parameters"]["search_parameter"]["cv"],
                scoring="f1",
                n_jobs=2,
            )
            xgb_RandomGrid.fit(self.X_train, self.y_train)
            print("#### cv_result f1 ####")
            print(xgb_RandomGrid.cv_results_["mean_test_score"])
            print("#### Best paramters ####")
            print(xgb_RandomGrid.best_params_)
            print(
                f"Train Accuracy - : {xgb_RandomGrid.score(self.X_train,self.y_train):.3f}"
            )
            best_model = xgb_RandomGrid.best_estimator_
            best_model.fit(self.X_train, self.y_train)
            joblib.dump(best_model, nombre_modelo, conf["parameters"]["compressor"])
            return best_model, xgb_RandomGrid.best_params_, nombre_modelo
        if conf["parameters"]["new"]:
            print("#### ENTRENANDO XGBOOST ####")
            model_xgboost = xgb.XGBRegressor(
                objective=conf["parameters"]["objective"],
                learning_rate=conf["parameters"]["learning_rate"],
                n_estimators=conf["parameters"]["estimators"],
                random_state=conf["parameters"]["random_state"],
            )
            model_xgboost.fit(self.X_train, self.y_train)
            joblib.dump(model_xgboost, nombre_modelo, conf["parameters"]["compressor"])
        return model_xgboost, None, nombre_modelo

    def entrenarSVM(self, conf):
        nombre_modelo = (
            "./models/"
            + conf["parameters"]["name"]
            + "_"
            + self.pair
            + "_"
            + self.timeframe
            + "_"
            + datetime.now().strftime("%d%m%Y%I%M%S%p")
            + ".joblib"
        )
        if conf["parameters"]["search_parameter"]["use"]:
            print("#### ENTRENANDO SVM CON GRIDSEARCH y CV ####")
            param_grid = {
                "kernel": conf["parameters"]["search_parameter"]["kernel"],
                "random_state": conf["parameters"]["search_parameter"]["random_state"],
            }
            grid_svm = svm.SVC()
            svm_RandomGrid = GridSearchCV(
                estimator=grid_svm,
                param_grid=param_grid,
                cv=conf["parameters"]["search_parameter"]["cv"],
                scoring="f1",
                n_jobs=2,
            )
            svm_RandomGrid.fit(self.X_train, self.y_train)
            print("#### cv_result f1 ####")
            print(svm_RandomGrid.cv_results_["mean_test_score"])
            print("#### Best paramters ####")
            print(svm_RandomGrid.best_params_)
            print(
                f"Train Accuracy - : {svm_RandomGrid.score(self.X_train,self.y_train):.3f}"
            )
            best_model = svm_RandomGrid.best_estimator_
            best_model.fit(self.X_train, self.y_train)
            joblib.dump(best_model, nombre_modelo, conf["parameters"]["compressor"])
            return best_model, svm_RandomGrid.best_params_, nombre_modelo
        if conf["parameters"]["new"]:
            print("#### ENTRENANDO SVM ####")
            model_SVM = svm.SVC(kernel=conf["parameters"]["kernel"], random_state=0)
            model_SVM.fit(self.X_train, self.y_train)
            joblib.dump(model_SVM, nombre_modelo, conf["parameters"]["compressor"])
        return model_SVM, None, nombre_modelo

    def getResumeResult(self):
        cur = self.conn.cursor()
        cur.execute(
            "SELECT id,model_name,f1,profit,tp,fp,n_buysacum,chart_fondos,chart_candles FROM MODEL WHERE train_id="
            + str(self.train_id)
        )
        rows = cur.fetchall()
        for row in rows:
            print(row)

    def getAllResult(self):
        cur = self.conn.cursor()
        cur.execute("SELECT * FROM MODEL WHERE train_id=" + str(self.train_id))
        rows = cur.fetchall()
        for row in rows:
            print(row)

    def testBestResult(self, column="profit"):
        """
        Parameters
        ----------
        column : string, optional
            Get the best train by "column" (ie. 'profit', 'f1' or 'roc'). The default is "profit".
            Then loads the trained models in an array (models) also load the scaler (scaler) used for training.
        Returns
        -------
        None.

        """
        cur = self.conn.cursor()
        command = "SELECT * FROM MODEL ORDER BY " + str(column) + " DESC LIMIT 1"
        cur.execute(command)
        rows = cur.fetchall()
        for row in rows:
            self.trained_models.append(
                {"name": row[2], "model": joblib.load(row[12]), "best_params": row[15],}
            )
        self.scaler = joblib.load(rows[0][13])
        config = self.configuration[0]
        config.backtests[0] = ast.literal_eval(str(rows[0][8]))
        config.target = ast.literal_eval(str(rows[0][9]))
        config.indicators = ast.literal_eval(str(rows[0][23]))
        print("--------- PREPARACION DE DATOS ---------")
        objPrepare = PrepareData(
            self.pair,
            self.timeframe,
            config.indicators,
            config.get_data["ini"],
            config.get_data["end"],
            config.prepare_data["date_split"],
            config.prepare_data["graph_start"],
            config.prepare_data["graph_end"],
            config.target,
        )

        self.backtests = config.backtests

        self.u_dataset = objPrepare.getUnknownDataset()
        self.x_columns = objPrepare.getXColumns()
        self.y_column = "resultado"
        self.only_oversell = config.prepare_data["only_oversell"]
        self.test_size = config.prepare_data["test_size"]
        self.random_state = config.prepare_data["random_state"]
        self.below_rsi = config.prepare_data["below_rsi"]
        self.train_name = "All"

        self.predictAllModels(self.u_dataset[self.x_columns], True)

        self.u_dataset["predecido"] = self.y_pred
        print("Predicted all")
        accu, prec, rec, spec, f1, roc, tn, tp, fn, fp = self.metrics(
            self.u_dataset[self.y_column], self.y_pred
        )
        (
            chart_fondos,
            chart_candles,
            backtest_name,
            backtest_conf,
            profit,
            n_buys,
            n_buysacum,
        ) = self.backtest(self.u_dataset, "All", self.backtests[0])
        print(self.backtests[0])
        sql = f"""INSERT INTO MODEL(train_id,model_name,model_file,scaler_file,parameters,best_params,
        f1,accuracy,precision,recall,specificity,roc,tn,tp,fn,fp,
        backtest_name,backtest_conf,indicator_conf, prepare_conf, target_conf,profit,n_buys,n_buysacum,
        chart_fondos,chart_candles, type_evaluation)
        VALUES({int(self.train_id)},"{str(column)}",'All models','All models','All models',
        'All models',{f1},{accu},{prec},{rec},{spec},{roc},{tn},{tp},{fn},
        {fp}, "{str(backtest_name)}", "{str(backtest_conf)}", "{str(config.indicators)}", "{str(config.prepare_data)}", "{str(config.target)}", {float(profit)},
        {int(n_buys)}, {int(n_buysacum)}, "{str(chart_fondos)}", "{str(chart_candles)}", 'All models')"""
        cur = self.conn.cursor()
        cur.execute(sql)
        self.conn.commit()
        return True

    def getBestModels(self, result_id):
        cur = self.conn.cursor()
        command = "SELECT * FROM MODEL WHERE id=" + str(result_id) + ""
        cur.execute(command)
        rows = cur.fetchall()
        models = []
        print(rows[0][2])
        if "All" in str(rows[0][2]):
            for mod in ast.literal_eval(str(rows[0][3])):
                print(mod)
                models.append(joblib.load(mod))
        else:
            models.append(joblib.load(rows[0][3]))
        return models

    def deleteTraineds(self, column="profit"):
        n_delete = 4  # len(self.configurations)*len(self.models)
        cur = self.conn.cursor()
        cur.execute(
            f"SELECT * FROM MODEL ORDER BY " + str(column) + f" ASC LIMIT {n_delete}"
        )
        rows = cur.fetchall()
        for row in rows:
            if "All" in str(rows[0][2]):
                for mod in ast.literal_eval(str(rows[0][3])):
                    if os.path.exists(mod):
                        os.remove(mod)
            else:
                if os.path.exists(rows[0][3]):
                    os.remove(rows[0][3])

        cur = self.conn.cursor()
        ide = str(rows[0][0])
        cur.execute(f"DELETE FROM MODEL WHERE id = {ide}")
        self.conn.commit()

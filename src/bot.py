# -*- coding: utf-8 -*-
"""
Created on Wed Apr 27 22:31:04 2022

@author: x4vyjm
"""
import sys
import time
import schedule
from src.saveData import saveData
from src.Trainer import Trainer
from src.Trader import Trader
from src.multiConfig import MultiConfig
import threading


class Bot:
    n_confs = 0
    bc = None
    pair = ""
    timeframe = ""
    conf = ""
    number_train = 1
    objTrainer = None
    objData = None
    objTrader = None
    time_new_training = 3
    config_name = ""

    def __init__(
        self,
        n_confs,
        binanceClient,
        pair,
        timeframe,
        conf,
        time_new_training,
        config_name,
    ):
        self.n_confs = n_confs
        self.config_name = config_name
        self.bc = binanceClient
        self.pair = pair
        self.timeframe = timeframe
        self.time_new_training = time_new_training
        self.conf = conf
        self.loadData()
        # self.objData.downloadData()

    def getConfigurations(self):
        print("--------- DEFINIENDO CONFIGURACIONES ---------")
        return MultiConfig(
            self.n_confs, self.timeframe, self.config_name
        ).getConfigurations()

    def loadData(self):
        print("--------- CARGA DE DATOS ---------")
        self.objData = saveData(
            self.pair,
            self.timeframe,
            self.conf.get_data["ini"],
            self.conf.get_data["end"],
            self.bc,
        )

    def train(self):
        print("### ENTRENAMIENTO")
        self.objTrainer = Trainer(
            self.getConfigurations(), self.pair, self.timeframe, self.number_train
        )
        self.objTrainer.train()
        self.number_train += 1

    def startTestBestModels(self):
        print("### TEST BEST MODELS")
        self.objTrainer = Trainer(
            self.getConfigurations(), self.pair, self.timeframe, self.number_train
        )
        self.objTrainer.testBestResult("f1")
        self.number_train += 1

    def trader(self):
        print("### TRADER INSTANCE")
        self.objTrader = Trader(self.bc, self.timeframe, self.pair, self.conf)

    def startTraderBot(self):
        print("### INICIANDO BOT")
        self.objData.loadDB()
        self.run_threaded(self.startGetCandle)
        # self.train()
        self.trader()
        checkLastCandle = schedule.Scheduler()
        newBestResult = schedule.Scheduler()
        # newTraining = schedule.Scheduler()
        checkLastCandle.every().minute.at(":02").do(self.objTrader.checkLastCandle)
        newBestResult.every(10).days.do(self.objTrader.getBestResult)
        # newTraining.every(self.time_new_training).minutes.do(self.objTrainer.train)
        while True:
            checkLastCandle.run_pending()
            newBestResult.run_pending()
            # newTraining.run_pending()
            time.sleep(1)

    def startTrainerBot(self):
        print("### INICIANDO TRAINER BOT")
        self.objData.loadDB()
        self.train()
        # newTraining = schedule.Scheduler()
        # newTraining.every(self.time_new_training).minutes.do(self.objTrainer.train)
        # while True:
        #    newTraining.run_pending()
        #    time.sleep(10)

    def startGetCandle(self):
        print("### Iniciando obtencion de ultima vela")
        getcandles = schedule.Scheduler()
        getcandles.every().minute.at(":00").do(self.objData.saveData)
        while True:
            getcandles.run_pending()
            time.sleep(1)

    def run_threaded(self, job_func):
        print("### Iniciando nuevo hilo de ejecución")
        job_thread = threading.Thread(target=job_func, daemon=True)
        job_thread.start()

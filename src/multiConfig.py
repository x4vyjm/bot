# -*- coding: utf-8 -*-
"""
Created on Tue Apr 19 18:49:05 2022

@author: x4vyjm
"""
from time import time
import config.config as config
import random


class MultiConfig:
    """
    Clase que genera archivos de configuración para enviarlos al entrenamiento
    """

    profit = {
        "5m": [0.01],
        "15m": [0.01, 0.015],
        "30m": [0.01, 0.02],
    }
    risk = {"5m": [0.02, 0.03], "15m": [0.02, 0.03], "30m": [0.02, 0.015]}
    profit_candles = {"5m": [8, 24, 48], "15m": [6, 12, 24], "30m": [6, 10, 16]}  #
    risk_candles = {"5m": [8, 24, 48], "15m": [6, 12, 24], "30m": [4, 8, 12]}  #
    min_rsi = {"5m": [28, 25], "15m": [28, 25], "30m": [25, 30]}  #

    # back_profit = [1.01, 1.02, 1.03]  # [1.01, 1.015, 1.02]
    back_max_buys = {
        "5m": [12, 20],
        "15m": [10, 14],
        "30m": [8, 12],
    }  # [10, 15, 20]  # [15, 20, 30]
    back_distance = {
        "5m": [48, 144],
        "15m": [16, 48],
        "30m": [12, 24],
    }  # [12, 24, 48, 96]  # [24, 48, 96, 144]
    configurations = []
    n_configurations = 1
    timeframe = ""
    config_name = ""

    def __init__(self, n_configurations, timeframe, config_name):
        """
        Parameters
        ----------
        n_configurations : integer
            Número de configuraciones que se generarán
        Returns
        -------
        None.
        """
        self.n_configurations = n_configurations
        self.timeframe = timeframe
        self.config_name = config_name
        self.setConfigurations()

    def setConfigurations(self):
        print("### Generando configuraciones")
        self.configurations.clear()
        for i in range(self.n_configurations):
            conf_aux = config.Config(self.config_name)
            conf_aux.target["profit"] = random.choice(self.profit[self.timeframe])
            conf_aux.target["risk"] = random.choice(self.risk[self.timeframe])
            conf_aux.target["profit_candles"] = random.choice(
                self.profit_candles[self.timeframe]
            )
            conf_aux.target["risk_candles"] = int(conf_aux.target["profit_candles"])
            conf_aux.target["min_rsi"] = random.choice(self.min_rsi[self.timeframe])
            conf_aux.backtests[0]["parameters"]["profit"] = (
                float(conf_aux.target["profit"])
            ) + 1
            max_n_buys = random.choice(self.back_max_buys[self.timeframe])
            conf_aux.backtests[0]["parameters"]["max_buys"] = int(max_n_buys)
            if int(max_n_buys) == 8:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 10
            elif int(max_n_buys) == 10:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 14
            elif int(max_n_buys) == 12:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 16
            elif int(max_n_buys) == 15:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 20
            elif int(max_n_buys) == 14:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 18
            elif int(max_n_buys) == 18:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 22
            elif int(max_n_buys) == 20:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 28
            elif int(max_n_buys) == 30:
                conf_aux.backtests[0]["parameters"]["initial_divisor"] = 40
            conf_aux.backtests[0]["parameters"]["distance"] = random.choice(
                self.back_distance[self.timeframe]
            )
            self.configurations.append(conf_aux)

    def getConfigurations(self, new=False):
        print("### Obteniendo configuraciones")
        """
        Parameters
        ----------
        new : boolean, optional
            Si es True generará nuevas configuraciones. The default is False.
        Returns
        -------
        List
            Lista con las configuraciones generadas
        """
        if new:
            self.setConfigurations()
        return self.configurations

    def updateNConfigurations(self, n_configurations):
        """
        Parameters
        ----------
        n_configurations : integer
            Nuevo número que define la cantidad de configuraciones a generar. Luego la nueva lista es generada.
        Returns
        -------
        None.

        """
        self.n_configurations = n_configurations
        self.setConfigurations()

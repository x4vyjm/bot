# -*- coding: utf-8 -*-
from ast import Try
from logging import raiseExceptions
import os
import json
import pandas as pd
import numpy as np
from sqlalchemy import Column
import talib as ta
import sqlite3 as sql
import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots


class PrepareData:
    """
    Clase con la cual se agregarán indicadores al dataset del tick dado.
    además se agregara la columna target. Esta clase devolverá el dataset listo para el entrenamiento
    """

    indicators = []
    target = []
    dataframe = None
    tick = ""
    timeframe = "5m"
    start = "01-01-2017"
    end = "31-12-2022"
    graph_start = "2021-10-1"
    graph_end = "2021-11-1"
    split = "01-11-2021"
    db_name = ""
    conn = None
    target = {}
    x_columns = ["close", "volume"]

    def __init__(
        self,
        tick,
        timeframe,
        indicators,
        start=None,
        end=None,
        split=None,
        graph_start=None,
        graph_end=None,
        target_dict=None,
        limit=False,
    ):
        self.tick = tick
        self.timeframe = timeframe
        self.indicators = indicators
        self.start = start
        self.end = end
        self.graph_start = graph_start
        self.graph_end = graph_end
        self.split = split
        self.target = target_dict
        self.db_name = "./database/" + self.tick + self.timeframe + ".db"
        self.conn = sql.connect(self.db_name, check_same_thread=False)
        self.loadDataset(limit)
        if split != None:
            self.addTarget()

    def loadDataset(self, limit=False):
        self.x_columns = ["close", "volume"]
        self.dataframe = pd.DataFrame(None)
        if limit:
            print(f"### Leyendo datos del pair con limite")
            command = (
                "select * from " + self.tick + " ORDER BY open_time DESC LIMIT 800"
            )
            self.dataframe = pd.read_sql_query(command, self.conn).iloc[::-1]
        else:
            print(f"### Leyendo datos del pair sin limite")
            command = "select * from " + self.tick
            self.dataframe = pd.read_sql_query(command, self.conn)
        self.addIndicators()

    def addIndicators(self):
        print(f"### Añadiendo indicadores al dataset")
        for indicator in self.indicators:
            if indicator == "rsi":
                self.dataframe["rsi"] = ta.RSI(self.dataframe["close"], timeperiod=14)
                self.x_columns.append("rsi")
            elif indicator == "obv":
                self.dataframe["obv"] = ta.OBV(
                    self.dataframe["close"], self.dataframe["volume"]
                )
                self.x_columns.append("obv")
            elif indicator == "adx":
                self.dataframe["adx"] = ta.ADX(
                    self.dataframe["high"],
                    self.dataframe["low"],
                    self.dataframe["close"],
                    timeperiod=14,
                )
                self.x_columns.append("adx")
            elif indicator == "macd":
                (
                    self.dataframe["macd"],
                    self.dataframe["macd_signal"],
                    self.dataframe["macd_hist"],
                ) = ta.MACD(
                    self.dataframe["close"],
                    fastperiod=12,
                    slowperiod=26,
                    signalperiod=9,
                )
                self.x_columns.append("macd")
                self.x_columns.append("macd_signal")
                self.x_columns.append("macd_hist")
            elif indicator == "ema12":
                self.dataframe["ema12"] = ta.EMA(self.dataframe["close"], timeperiod=12)
                self.x_columns.append("ema12")
            elif indicator == "ema24":
                self.dataframe["ema24"] = ta.EMA(self.dataframe["close"], timeperiod=24)
                self.x_columns.append("ema24")
            elif indicator == "ema50":
                self.dataframe["ema50"] = ta.EMA(self.dataframe["close"], timeperiod=50)
                self.x_columns.append("ema50")
            elif indicator == "ema100":
                self.dataframe["ema100"] = ta.EMA(
                    self.dataframe["close"], timeperiod=100
                )
                self.x_columns.append("ema100")
            elif indicator == "ema200":
                self.dataframe["ema200"] = ta.EMA(
                    self.dataframe["close"], timeperiod=200
                )
                self.x_columns.append("ema200")
            elif indicator == "ema400":
                self.dataframe["ema400"] = ta.EMA(
                    self.dataframe["close"], timeperiod=400
                )
                self.x_columns.append("ema400")
            elif indicator == "change":
                self.dataframe["change"] = (
                    (self.dataframe["close"] - self.dataframe["open"])
                    / self.dataframe["open"]
                ) * 100
                self.x_columns.append("change")
        self.dataframe = self.dataframe.dropna()

    def f(self, df, threshold=1):
        out = []
        for col in df.columns:
            m = df[col].eq(1)
            g = (df[col] != df[col].shift()).cumsum()[m]
            mask = g.groupby(g).transform("count").ge(threshold)
            filt = g[mask].reset_index()
            output = filt.groupby(col)["index"].agg(["first", "last"])
            output.insert(0, "col", col)
            out.append(output)
        return pd.concat(out, ignore_index=True)

    def addTarget(self):
        if self.target["target_min"]:
            print(
                f"####  CREANDO COLUMNA OBJETIVO CON MINIMO ENTRE RANGOS DE RSI<{self.target['min_rsi']} ####"
            )
            dsaux = pd.DataFrame()
            dsaux["aux"] = np.where(
                self.dataframe["rsi"] <= self.target["min_rsi"], 1, 0
            )
            df = self.f(dsaux[["aux"]])
            print(df)
            print("####  iniciar comparacion futura 1111 ####")
            indexes_target_true = []
            for ind in df.index:
                # Extrae el mínimo precio de cierre en el rango de indices
                try:
                    if df["first"][ind] > 200:
                        indice, column = (
                            self.dataframe[["close"]]
                            .stack()
                            .loc[df["first"][ind] : df["last"][ind]]
                            .idxmin()
                        )
                        indexes_target_true.append(indice)
                except Exception as e:
                    print(e)
                    raiseExceptions()
            self.dataframe["resultado"] = False
            contador = 0
            for ind in indexes_target_true:
                aux = ind + self.target["profit_candles"]
                precio_max = self.dataframe[["close"]].loc[ind:aux].max()
                if (
                    self.dataframe["close"][ind].item() * self.target["profit"]
                    <= precio_max.item()
                ):
                    self.dataframe.at[ind, "resultado"] = True
                    contador += 1
            print("MINIM ARRAY: ", len(indexes_target_true))
            print("REBOTES %: ", contador)

        else:
            print("####  CREANDO COLUMNA OBJETIVO CON PROFIT Y STOP EN N VELAS ####")
            # ¿dentro de las 6 siguientes velas, si comprara en precio de cierre, alcanza un "beneficio"% de ganancia?
            datasetclose = pd.DataFrame()
            datasetclose["close"] = self.dataframe["close"]
            datasetclose["high"] = self.dataframe["high"]
            datasetclose["result1"] = False
            for n in range(self.target["profit_candles"]):
                datasetclose["aux"] = datasetclose["high"].shift(-1 * (n + 1))
                datasetclose["result1"] = datasetclose["result1"] | (
                    np.where(
                        datasetclose["aux"]
                        >= datasetclose["close"] * (1 + self.target["profit"]),
                        True,
                        False,
                    )
                )

            # ¿dentro de las "n_riesgo" siguientes velas, si comprara en precio de cierre, perderé "riesgo"% ?
            datasetclose["result2"] = True
            for n in range(self.target["risk_candles"]):
                datasetclose["aux"] = datasetclose["close"].shift(-1 * (n + 1))
                datasetclose["result2"] = datasetclose["result2"] & (
                    np.where(
                        datasetclose["aux"]
                        <= datasetclose["close"] * (1 - self.target["risk"]),
                        False,
                        True,
                    )
                )
            # ¿El rsi (del 1 al 100, siendo 70 sobrecompra y 30 sobreventa) esta por debajo de "rsi_min" ?
            datasetclose["resultrsi"] = np.where(
                self.dataframe["rsi"] <= self.target["min_rsi"], True, False
            )

            if self.target["use_histogram_reverse"]:
                datasetclose["histopositivo"] = np.where(
                    (
                        (
                            self.dataframe["macd_hist"].shift(-1)
                            > self.dataframe["macd_hist"]
                        )
                        & (self.dataframe["macd_hist"].shift(-1) < 0)
                    ),
                    True,
                    False,
                )
                self.dataframe["resultado"] = (
                    datasetclose["result1"]
                    & datasetclose["result2"]
                    & datasetclose["resultrsi"]
                    & datasetclose["histopositivo"]
                )
            else:
                self.dataframe["resultado"] = (
                    datasetclose["result1"]
                    & datasetclose["result2"]
                    & datasetclose["resultrsi"]
                )
        self.dataframe["open_time"] = pd.to_datetime(
            self.dataframe["open_time"], unit="ms"
        )
        self.dataframe["close_time"] = pd.to_datetime(
            self.dataframe["close_time"], unit="ms"
        )
        self.dataframe = self.dataframe.set_index("open_time")
        print("###### Columnas despues de poner target: ", self.dataframe.columns)

    def showGraph(self):
        print(f"### Imprimiendo grafico del dataset")
        dataset1 = self.dataframe.loc[self.graph_start : self.graph_end]
        dataset1 = dataset1.reset_index()
        fig = make_subplots(rows=2, cols=1, shared_xaxes=True, vertical_spacing=0.02)
        fig.add_trace(
            go.Candlestick(
                x=dataset1["open_time"],
                open=dataset1["open"],
                high=dataset1["high"],
                low=dataset1["low"],
                close=dataset1["close"],
            ),
            row=1,
            col=1,
        )
        fig.add_trace(
            go.Scatter(x=dataset1["open_time"], y=dataset1["rsi"]), row=2, col=1
        )
        win = 0
        for i in range(len(dataset1["resultado"])):
            temp = dataset1.loc[i:i, "resultado"].values[0]
            # temp1 = dataset1.loc[i:i,'Rsi'].values[0]
            if temp == True:
                fig.add_annotation(
                    x=dataset1.loc[i, "open_time"],
                    y=dataset1.loc[i, "close"],
                    text="▲",
                    showarrow=False,
                    font=dict(size=16, color="blue"),
                )
                win += 1
            # if temp1<target['min_rsi']:
            #    fig.add_annotation(x=dataset1.loc[i,'Open time'], y=dataset1.loc[i,'Close'], text="▲", showarrow=False, font=dict(size=12, color='green'))
        fig.update_layout(xaxis_rangeslider_visible=False, showlegend=True)
        fig.write_html(f"./data/graphs/grafico_data_{self.tick}.html")
        fig.show()

    def getTrainDataset(self):
        print(f"### Obteniendo dataset de entrenamiento")
        return self.dataframe.loc[self.start : self.split]

    def getLastRow(self):
        print(f"### Obteniendo última fila del dataset")
        return self.dataframe.iloc[-1:]

    def getUnknownDataset(self):
        print(f"### Obteniendo dataset desconocido")
        return self.dataframe.loc[self.split : self.end]

    def getDataset(self):
        print(f"### Obteniendo dataset completo")
        return self.dataframe

    def getXColumns(self):
        print("### Obteniendo columnas x")
        return self.x_columns

    def getXColumnsExceptPrice(self):
        print("### Obteniendo columnas x")
        return self.x_columns[1:]

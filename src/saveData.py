# -*- coding: utf-8 -*-
import sqlite3 as sql
import pandas as pd
import os
import datetime
import json


class saveData:
    pair = ""
    timeframe = ""
    binanceClient = None
    db_name = None
    file_name = ""
    conn = None
    start = "21-03-2022"
    end = "28-03-2022"

    def __init__(self, tick, timeframe, start, end, binanceClient):
        self.pair = tick
        self.timeframe = timeframe
        self.binanceClient = binanceClient
        # self.loop = asyncio.get_event_loop()
        self.db_name = "./database/" + self.pair + self.timeframe + ".db"
        self.file_name = self.pair + self.timeframe + ".json"
        self.conn = sql.connect(self.db_name, check_same_thread=False)
        self.start = start
        self.end = end

    def loadDB(self):
        print(f"### Cargando data en la tabla PAIR {self.pair}")
        cur = self.conn.cursor()
        command = "DROP TABLE IF EXISTS " + self.pair
        cur.execute(command)
        command = (
            "CREATE TABLE "
            + self.pair
            + " (open_time datetime primary key, open float, high float, low float, close float, volume double, close_time datetime)"
        )
        cur.execute(command)
        self.conn.commit()
        df = self.getHistoricalData()
        df1 = self.getLastMonthData()
        df2 = self.getLastDayData()
        df = pd.concat([df.head(-1), df1], axis=0)
        df = pd.concat([df.head(-1), df2.head(-1)], axis=0)
        df = df.set_index("open_time")
        df = df[~df.index.duplicated(keep="first")]
        df = df.reset_index()
        df[
            ["open_time", "open", "high", "low", "close", "volume", "close_time"]
        ].to_sql(self.pair, self.conn, if_exists="append", index=False)
        df = df.iloc[0:0]

    def downloadData(self):
        print(f"### Descargando {self.pair} DATA")
        klines = self.binanceClient.get_historical_klines(
            self.pair, interval=self.timeframe, start_str=self.start, end_str=self.end
        )
        fi = open("./data/ticks/" + self.file_name, "w")
        fi.write(json.dumps(klines))
        fi.close()
        return True

    def getHistoricalData(self):
        print(f"### Obteniendo data historica {self.pair} ")
        file_exists = os.path.exists("./data/ticks/" + self.file_name)
        if not file_exists:
            klines = self.binanceClient.get_historical_klines(
                self.pair,
                interval=self.timeframe,
                start_str=self.start,
                end_str=self.end,
            )
            fi = open("./data/ticks/" + self.file_name, "w")
            fi.write(json.dumps(klines))
            fi.close()
        print("### Leyendo datos de archivo: ", self.file_name)
        df = pd.read_json("./data/ticks/" + self.file_name)
        df.columns = [
            "open_time",
            "open",
            "high",
            "low",
            "close",
            "volume",
            "close_time",
            "Quote asset volume",
            "Number of trades",
            "Taker buy base asset volume",
            "Taker buy quote asset volume",
            "ignore",
        ]
        df = df.drop(
            [
                "Quote asset volume",
                "Number of trades",
                "Taker buy base asset volume",
                "Taker buy quote asset volume",
                "ignore",
            ],
            axis=1,
        )

        return df

    def getLastMonthData(self):
        print(f"### Obteniendo data del ultimo mes {self.pair}")
        start = datetime.date.today() - datetime.timedelta(days=5)
        df = pd.DataFrame(
            self.binanceClient.get_historical_klines(
                self.pair,
                interval=self.timeframe,
                start_str=str(start),
                end_str=str(datetime.date.today()),
            )
        )

        df.columns = [
            "open_time",
            "open",
            "high",
            "low",
            "close",
            "volume",
            "close_time",
            "Quote asset volume",
            "Number of trades",
            "Taker buy base asset volume",
            "Taker buy quote asset volume",
            "ignore",
        ]
        df = df.drop(
            [
                "Quote asset volume",
                "Number of trades",
                "Taker buy base asset volume",
                "Taker buy quote asset volume",
                "ignore",
            ],
            axis=1,
        )
        return df

    def getLastDayData(self):
        print(f"### Obteniendo data del dia {self.pair}")
        df = pd.DataFrame(
            self.binanceClient.get_historical_klines(
                self.pair, self.timeframe, "1 day ago UTC"
            )
        )
        df.columns = [
            "open_time",
            "open",
            "high",
            "low",
            "close",
            "volume",
            "close_time",
            "Quote asset volume",
            "Number of trades",
            "Taker buy base asset volume",
            "Taker buy quote asset volume",
            "ignore",
        ]
        df = df.drop(
            [
                "Quote asset volume",
                "Number of trades",
                "Taker buy base asset volume",
                "Taker buy quote asset volume",
                "ignore",
            ],
            axis=1,
        )
        return df

    def getCandles(self, n_last_candles):
        print(f"### Obteniendo n velas de la BD {self.pair}")
        cur = self.conn.cursor()
        command = (
            "SELECT * from "
            + self.pair
            + " ORDER BY open_time DESC LIMIT "
            + str(n_last_candles)
        )
        cur.execute(command)
        return cur.fetchall()

    def saveData(self):
        now = datetime.datetime.now()
        time_divisor = {"5m": 5, "30m": 30, "15m": 15, "1h": 60}
        if now.minute % time_divisor[self.timeframe] == 0:
            print(f"### Obteniendo nueva vela {self.pair}")
            delta = datetime.timedelta(minutes=time_divisor[self.timeframe])
            final_time = datetime.datetime.now() - delta
            unix_timestamp = (
                datetime.datetime.timestamp(
                    datetime.datetime.strptime(
                        final_time.strftime("%Y-%m-%d %H:%M:00"), "%Y-%m-%d %H:%M:00",
                    )
                )
                * 1000
            )
            candles = self.binanceClient.get_klines(
                symbol=self.pair, interval=str(self.timeframe), limit=3
            )

            cd = pd.DataFrame(candles)
            cd.columns = [
                "open_time",
                "open",
                "high",
                "low",
                "close",
                "volume",
                "close_time",
                "Quote asset volume",
                "Number of trades",
                "Taker buy base asset volume",
                "Taker buy quote asset volume",
                "ignore",
            ]
            cd = cd.drop(
                [
                    "Quote asset volume",
                    "Number of trades",
                    "Taker buy base asset volume",
                    "Taker buy quote asset volume",
                    "ignore",
                ],
                axis=1,
            )
            cd[
                ["open_time", "open", "high", "low", "close", "volume", "close_time"]
            ].loc[cd["open_time"] == int(unix_timestamp)].to_sql(
                self.pair, self.conn, if_exists="append", index=False
            )
        return True


import config.config as config
from binance import Client
from src.bot import Bot
import sys

# import nest_asyncio
# nest_asyncio.apply()
def main():
    try:
        sett = config.Config(str(sys.argv[1]))
        ticks = sett.get_data["ticks"]
        binanceClient = Client(sett.api_key, sett.api_secret)
        timeframe = sett.get_data["timeframe"]
        iterations = 2
        while iterations:
            for tick in ticks:
                objBot = Bot(
                    10, binanceClient, tick, timeframe, sett, 30, str(sys.argv[1])
                )
                objBot.startTrainerBot()
            iterations -= 1

    except ValueError as ve:
        return str(ve)


if __name__ == "__main__":
    main()

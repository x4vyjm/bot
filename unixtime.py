import time
import datetime

delta = datetime.timedelta(minutes=5)
final_time = datetime.datetime.now() - delta
unix_timestamp = (
    datetime.datetime.timestamp(
        datetime.datetime.strptime(
            final_time.strftime("%Y-%m-%d %H:%M:00"), "%Y-%m-%d %H:%M:00",
        )
    )
    * 1000
)
print(int(unix_timestamp))
